import numpy as np
from PIL import Image

def makeOdd(number):
	"""
		Returns the closest odd number to number
		LSB of odd number is 1
	"""
	if(number&1==0):
		return number + 1
	else:
		return number

def makeEven(number):
	"""
		Returns the closest even number to number
		LSB of even number is 0
	"""
	if(number&1==0):
		return number
	else:
		return number - 1

def getLSB(number):
	"""
		Returns the Least Significant Bit of byte
	"""
	return number&1

def increment(data, x, y, color):
	"""
		Returns the coordinates, color for the next byte in the image.
	"""
	if(color < len(data[x][y]) - 1):
		color = color + 1
		return x, y, color
	else:
		color = 0
		if(y < len(data[x]) - 1):
			y = y + 1
			return x, y, color
		else:
			y = 0
			if(x < len(data) - 1):
				x = x + 1
				return x, y, color
			else:
				print "Message too long"
				return x, y, color


def hide(image, message, out):
	"""
		opens image
		writes message to image
		saves the image with the message as out
	"""
	message = '\0' + message + '\0'
	bytes = bytearray(message)
	im = Image.open(image)
	data = np.array(im)
	x = y = color = 0
	for byte in bytes:
		if(byte & 128 != 0):
			data[x][y][color] = makeOdd(data[x][y][color])
		else:
			data[x][y][color] = makeEven(data[x][y][color])
		x, y, color = increment(data, x, y, color)
		if(byte & 64 != 0):
			data[x][y][color] = makeOdd(data[x][y][color])
		else:
			data[x][y][color] = makeEven(data[x][y][color])
		x, y, color = increment(data, x, y, color)
		if(byte & 32 != 0):
			data[x][y][color] = makeOdd(data[x][y][color])
		else:
			data[x][y][color] = makeEven(data[x][y][color])
		x, y, color = increment(data, x, y, color)
		if(byte & 16 != 0):
			data[x][y][color] = makeOdd(data[x][y][color])
		else:
			data[x][y][color] = makeEven(data[x][y][color])
		x, y, color = increment(data, x, y, color)
		if(byte & 8 != 0):
			data[x][y][color] = makeOdd(data[x][y][color])
		else:
			data[x][y][color] = makeEven(data[x][y][color])
		x, y, color = increment(data, x, y, color)
		if(byte & 4 != 0):
			data[x][y][color] = makeOdd(data[x][y][color])
		else:
			data[x][y][color] = makeEven(data[x][y][color])
		x, y, color = increment(data, x, y, color)
		if(byte & 2 != 0):
			data[x][y][color] = makeOdd(data[x][y][color])
		else:
			data[x][y][color] = makeEven(data[x][y][color])
		x, y, color = increment(data, x, y, color)
		if(byte & 1 != 0):
			data[x][y][color] = makeOdd(data[x][y][color])
		else:
			data[x][y][color] = makeEven(data[x][y][color])
		x, y, color = increment(data, x, y, color)
	im = Image.fromarray(data)
	im.save(out)

def show(image):
	"""
		opens image
		checks for hidden message
		prints hidden message
	"""
	im = Image.open(image)
	data = np.array(im)
	x = y = color = 0
	byte = 0
	byte = byte + 128 * getLSB(data[x][y][color])
	x, y, color = increment(data, x, y, color)
	byte = byte + 64 * getLSB(data[x][y][color])
	x, y, color = increment(data, x, y, color)
	byte = byte + 32 * getLSB(data[x][y][color])
	x, y, color = increment(data, x, y, color)
	byte = byte + 16 * getLSB(data[x][y][color])
	x, y, color = increment(data, x, y, color)
	byte = byte + 8 * getLSB(data[x][y][color])
	x, y, color = increment(data, x, y, color)
	byte = byte + 4 * getLSB(data[x][y][color])
	x, y, color = increment(data, x, y, color)
	byte = byte + 2 * getLSB(data[x][y][color])
	x, y, color = increment(data, x, y, color)
	byte = byte + 1 * getLSB(data[x][y][color])
	x, y, color = increment(data, x, y, color)
	if byte!=0:
		print "No message found"
	else:
		message = ""
		while True:
			byte = 0
			byte = byte + 128 * getLSB(data[x][y][color])
			x, y, color = increment(data, x, y, color)
			byte = byte + 64 * getLSB(data[x][y][color])
			x, y, color = increment(data, x, y, color)
			byte = byte + 32 * getLSB(data[x][y][color])
			x, y, color = increment(data, x, y, color)
			byte = byte + 16 * getLSB(data[x][y][color])
			x, y, color = increment(data, x, y, color)
			byte = byte + 8 * getLSB(data[x][y][color])
			x, y, color = increment(data, x, y, color)
			byte = byte + 4 * getLSB(data[x][y][color])
			x, y, color = increment(data, x, y, color)
			byte = byte + 2 * getLSB(data[x][y][color])
			x, y, color = increment(data, x, y, color)
			byte = byte + 1 * getLSB(data[x][y][color])
			x, y, color = increment(data, x, y, color)
			if(byte==0):
				break
			else:
				message = message + chr(byte)
		print message

